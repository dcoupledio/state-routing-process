<?php namespace Decoupled\Core\Bundle\Process;

use Decoupled\Core\Bundle\BundleProcess;
use Decoupled\Core\Bundle\BundleInterface;
use Decoupled\Core\Application\ApplicationContainer;

class StateRoutingProcess extends BundleProcess{

    public function __construct( ApplicationContainer $app )
    {
        $this->setApp( $app );
    }

    public function getName()
    {
        return 'state.routing';
    }

    public function process( BundleInterface $bundle )
    {
        $dir = $bundle->getDir();

        $statesPath = $dir.DIRECTORY_SEPARATOR.'/states.php';

        $action = $this->getActionFromFile( $statesPath );

        if(!$action) return;

        $action();
    }
}
