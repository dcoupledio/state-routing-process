<?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\Extension\Bundle\BundleExtension;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\State\StateRouterExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Core\Bundle\Process\StateRoutingProcess;
use Decoupled\Core\Bundle\Process\Test\AppBundle;

class ProcessTest extends TestCase{

    public function testCanUseProcess()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new BundleExtension() );

        $app->uses( new DependencyInjectionExtension() );

        $app->uses( new StateRouterExtension() );

        $app->bundle()->add( new AppBundle() );

        $app->uses(function( $bundleInitializer ){

            $bundleInitializer->uses( new StateRoutingProcess($this) );

        });

        $app->process()->init( $app->bundle() );

        $states = $app->state()->getCollection();

        $this->assertNotEmpty( $states );

        $state = $states->get('test.route');

        $this->assertNotNull( $state );

        $this->assertContains( 'a', $state->getStates() );
    }

}